# alta3research-gitlab

Alta3 Research Git and GitLab Certification Project

### Description

This project has been created to show what I have learned about how to use Git & Gitlab.

### Examples

Here's a list of the knowledge demonstrated in this project

- Wiki creation
- Piplines
- Writing documentation in Markdown
- Adding collaborators
- Creating Webhooks
- Milestones
